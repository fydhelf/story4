from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.story1, name='story1'),
    path('home', views.home, name='home'),
    path('profile', views.profile, name='profile'),
    path('contact', views.contact, name='contact'),
    path('experience', views.experience, name='experience'),
    path('matkuls', views.matkuls, name='matkuls'),
    path('hasil', views.hasil, name='hasil'),
    path('hapus/<str:pk>/', views.hapus, name='hapus'),
    path('tambahkegiatan', views.tambahkegiatan, name='tambahkegiatan'),
    path('daftarkegiatan', views.daftarkegiatan, name='daftarkegiatan'),
    path('hapuskegiatan/<str:pk>/', views.hapuskegiatan, name='hapuskegiatan'),
    path('tambahnama/<int:id>', views.tambahnama, name='tambahnama'),
    path('hapusnama/<str:pk>/', views.hapusnama, name='hapusnama'),
    path('story7', views.story7, name='story7'),
    path('story8', views.story8, name='story8'),
    path('data/', views.data, name='data'),
]