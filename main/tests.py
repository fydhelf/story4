from main.views import contact, daftarkegiatan, experience, hapus, hapuskegiatan, hapusnama, hasil, home, matkuls, profile, story1, tambahkegiatan, tambahnama 
from django.urls.base import resolve
from main.models import matkul, pendaftar, tambah
from django.test import LiveServerTestCase, TestCase, tag
from django.test import Client
from django.urls import reverse
from selenium import webdriver
from main.apps import MainConfig


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_url_story1(self):
        response = self.client.get(reverse('main:story1'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')
    
    def test_url_home(self):
        response = self.client.get(reverse('main:home'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_url_profile(self):
        response = self.client.get(reverse('main:profile'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_url_contact(self):
        response = self.client.get(reverse('main:contact'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_url_experience(self):
        response = self.client.get(reverse('main:experience'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_url_matkul(self):
        response = self.client.get(reverse('main:matkuls'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_url_hasil(self):
        response = self.client.get(reverse('main:hasil'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')
    
    def test_story6_url_tambahkegiatan(self):
        response = self.client.get(reverse('main:tambahkegiatan'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_story6_url_daftarkegiatan(self):
        response = self.client.get(reverse('main:daftarkegiatan'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_story7(self):
        response = self.client.get(reverse('main:story7'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_story8(self):
        response = self.client.get(reverse('main:story8'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'base.html')

    def test_model(self):
        matkul.objects.create(nama ="a" ,dosen = "a" ,sks ="a" ,deskripsi ="a" ,tahun ="a" ,kelas = "a")
        tambah.objects.create(namakegiatan = "a")
        obj1 = tambah.objects.get(id=1)
        pendaftar.objects.create(kegiatan=obj1 ,namapendaftar = "a")
        hitungmatkul = matkul.objects.all().count()
        hitungtambah = tambah.objects.all().count()
        hitungpendaftar = pendaftar.objects.get(id=1)
        self.assertEquals(hitungmatkul,1)
        self.assertEquals(hitungtambah,1)
        self.assertEquals(str(hitungpendaftar),"a")

    def test_views_home(self):
        found = resolve('/home')            
        self.assertEqual(found.func, home)

    def test_views_profile(self):
        found = resolve('/profile')            
        self.assertEqual(found.func, profile)

    def test_views_contact(self):
        found = resolve('/contact')            
        self.assertEqual(found.func, contact)

    def test_views_experience(self):
        found = resolve('/experience')            
        self.assertEqual(found.func, experience)

    def test_views_matkuls(self):
        found = resolve('/matkuls')            
        self.assertEqual(found.func, matkuls)

    def test_views_hasil(self):
        found = resolve('/hasil')            
        self.assertEqual(found.func, hasil)

    def test_views_tambahkegiatan(self):
        found = resolve('/tambahkegiatan')            
        self.assertEqual(found.func, tambahkegiatan)

    def test_views_daftarkegiatan(self):
        found = resolve('/daftarkegiatan')            
        self.assertEqual(found.func, daftarkegiatan)

class MainConfigTest(TestCase):
    def test_app(self):
        self.assertEqual(MainConfig.name, 'main')



class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
