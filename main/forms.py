from django import forms
from django.forms import ModelForm
from django.forms import fields
from django.forms import widgets
from .models import matkul, pendaftar,tambah

class tambahkan(ModelForm):
    class Meta:
        model = matkul;
        fields = '__all__'
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
            'dosen' : forms.TextInput(attrs={'class': 'form-control'}),
            'sks' : forms.TextInput(attrs={'class': 'form-control'}),
            'deskripsi' : forms.TextInput(attrs={'class': 'form-control'}),
            'tahun' : forms.TextInput(attrs={'class': 'form-control'}),
            'kelas' : forms.TextInput(attrs={'class': 'form-control'}),
        }

class tambahkankegiatan(ModelForm):
    class Meta:
        model = tambah;
        fields = '__all__'
        widgets = {
            'namakegiatan' : forms.TextInput(attrs={'class': 'form-control'}),
        }

class tambahkannama(ModelForm):
    class Meta:
        model = pendaftar;
        fields = '__all__'
        widgets = {
            'namapendaftar' : forms.TextInput(attrs={'class': 'form-control'}),
        }