from django.db import models
from django.utils import timezone
from datetime import datetime, date

class matkul(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=30)
    tahun = models.CharField(max_length=30)
    kelas = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class tambah(models.Model):
    namakegiatan = models.CharField(max_length=30)

    def listPeserta(self):
        return pendaftar.objects.filter(kegiatan=self.id)

    peserta = property(listPeserta)

    def __str__(self):
        return self.namakegiatan

class pendaftar(models.Model):
    kegiatan = models.ForeignKey(tambah, null=True,on_delete=models.CASCADE)
    namapendaftar = models.CharField(max_length=30)

    def __str__(self):
        return self.namapendaftar

# Create your models here.
