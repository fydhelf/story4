from django.shortcuts import render, redirect
from .models import pendaftar, matkul, tambah
from .forms import tambahkan, tambahkankegiatan, tambahkannama
from django.http.response import JsonResponse
from pip._vendor import requests
import json

def home(request):
    return render(request, 'main/home.html')
def story1(request):
    return render(request, 'main/story1.html')
def profile(request):
    return render(request, 'main/profile.html')
def contact(request):
    return render(request, 'main/contact.html')
def experience(request):
    return render(request, 'main/experience.html')
def matkuls(request):
    form = tambahkan()
    context = {'form':form}
    if request.method == 'POST':
        form = tambahkan(request.POST)
        form_class = tambahkan
        if form.is_valid():
            form.save()
            return redirect('/hasil')
    return render(request, 'main/matkul.html',context)
def hasil(request):
    result = matkul.objects.all()
    context = {'result':result}
    return render(request, 'main/hasil.html',context)
def hapus(request,pk):
    hapus = matkul.objects.get(id=pk)
    hapus.delete()
    return redirect('/hasil')

def tambahkegiatan(request):
    form = tambahkankegiatan()
    context = {'form':form}
    if request.method == 'POST':
        form = tambahkankegiatan(request.POST)
        form_class = tambahkankegiatan
        if form.is_valid():
            post = form.save(commit=True)
            post.save()
            return redirect('/daftarkegiatan')
    return render(request, 'main/tambahkegiatan.html',context)
def daftarkegiatan(request):
    result = tambah.objects.all()
    context = {'result':result}
    return render(request, 'main/daftarkegiatan.html',context)

def hapuskegiatan(request,pk):
    hapus = tambah.objects.get(id=pk)
    hapus.delete()
    return redirect('/daftarkegiatan')

def tambahnama(request,id):
    daftar = tambah.objects.get(id=id)
    forms = tambahkannama(request.POST, instance=daftar)
    if forms.is_valid():
        data = pendaftar()
        data.kegiatan = forms.cleaned_data['kegiatan']
        data.namapendaftar = forms.cleaned_data['namapendaftar']
        data.save()
        return redirect('/daftarkegiatan')
    context = {'forms':forms, 'daftar':daftar}
    return render(request, 'main/tambahnama.html',context)

def hapusnama(request,pk):
    hapus = pendaftar.objects.get(id=pk)
    hapus.delete()
    return redirect('/daftarkegiatan')

def story7(request):
    return render(request, 'main/story7.html')

def story8(request):
    response = {}
    return render(request, 'main/story8.html', response)

def data(request):
    url="https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret=requests.get(url)

    data=json.loads(ret.content)
    return JsonResponse(data,safe=False)