from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from Story9.apps import Story9Config
from .views import *
from .urls import *
# Create your tests here.

# TEST APP
class Story9ConfigTest(TestCase):
    def test_app(self):
        self.assertEqual(Story9Config.name, 'Story9')

#TEST HOME
class Story9test(TestCase):
    def test_url_home(self):
        response = Client().get('/Story9/')
        self.assertEqual(response.status_code, 200)

    def test_home_template(self):
        response = Client().get('/Story9/')
        self.assertTemplateUsed(response, 'Story9.html')

    def test_home_func(self):
        found = resolve('/Story9/')
        self.assertEqual(found.func, home)
    
    def test_view_dalam_template_home(self):
        response = Client().get('/Story9/')  
        isi_view = response.content.decode('utf8')
        self.assertIn("Selamat Datang!", isi_view)   

#TEST SIGNUP
class SignUpTests(TestCase):
    def setUp(self):
        self.username = 'a'
        self.password = 'Abc123'

    def test_url_signup(self):
        response = self.client.get("/Story9/signup/")
        self.assertEqual(response.status_code, 200)

    def test_signup_template(self):
        response = self.client.get("/Story9/signup/")
        self.assertTemplateUsed(response, 'signup.html')

    def test_signup_form(self):
        response = self.client.post("/Story9/signup/", 
        data={
            'username': self.username,
            'password1': self.password,
            'password2': self.password
        })
        self.assertEqual(response.status_code, 200)

#TEST LOGIN
class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'a',
            'password': 'Abc123'}
        User.objects.create_user(**self.credentials)
    
    def test_url_login(self):
        response = self.client.get("/Story9/login/")
        self.assertEqual(response.status_code, 200)
    
    def test_login_template(self):
        response = self.client.get("/Story9/login/")
        self.assertTemplateUsed(response, 'login.html')

    def test_login(self):
        response = self.client.post('/Story9/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)

#TEST LOGOUT    
class LogoutTest(TestCase):
   def test_logout(self):
        self.client = Client()
        self.client.login(username='a', password='Abc123')
        response = self.client.get('/Story9/logout/')
        self.assertEqual(response.status_code, 302)
