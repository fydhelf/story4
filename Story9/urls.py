from django.urls import path

from . import views

app_name = 'Story9'

urlpatterns = [
    path('Story9/', views.home, name ='home'),
    path('Story9/signup/', views.signup, name='signup'),
    path('Story9/login/', views.signin, name='login'),
    path('Story9/logout/', views.signout, name='logout'),
]